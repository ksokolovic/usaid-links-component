# USAID Universal LINKS Footer Information

![Dark theme](admin/images/screencap-dark-theme.png "Dark Theme")
Above, the "dark" theme, standard and hover states.

![Light theme](admin/images/screencap-light-theme.png "Light Theme")
Above, the "light" theme, standard and hover states.

## Getting Started
This module is for Drupal 8.9 and Drupal 9.x. If you are looking for
a solution for other platforms such as WordPress,
[please visit https://gitlab.com/kdlt/usaid-links-component-html](https://gitlab.com/kdlt/usaid-links-component-html)

You can download this module by installing via composer. First, add
the code snippet below in your composer.json file within the
`"repositories": [...` area or create one if you do not have this.

```json
     "type": "package",
            "package": {
                "name": "kdlt/usaid_link_sites",
                "version": "1.0.0",
                "type":"drupal-module",
                "source": {
                    "url": "https://gitlab.com/kdlt/usaid-links-component.git",
                    "type": "git",
                    "reference": "1.0.0"
                }
            }
```

As an example, once you add the code above, your entire repositories block might look like this:

```json
    "repositories": [
        {
            "type": "composer",
            "url": "https://packages.drupal.org/8"
        },
        {
            "type": "composer",
            "url": "https://asset-packagist.org"
        },
        {
            "type": "package",
            "package": {
                "name": "kdlt/usaid_link_sites",
                "version": "1.0.0",
                "type":"drupal-module",
                "source": {
                    "url": "https://gitlab.com/kdlt/usaid-links-component.git",
                    "type": "git",
                    "reference": "1.0.0"
                }
            }
        }
    ],
```

Once this is set, run the command below to download.

```bash
composer require kdlt/usaid_link_sites
```

## Usage
* Enable this module either via drush or the Drupal admin UI and then
  visit `/admin/config/usaid-link-sites/help` for detailed help information
  on how to use this module.

## Getting help
If you need help, [please contact Rachel Plett](mailto:rachel.plett@bixal.com) at Bixal Solutions Inc.

