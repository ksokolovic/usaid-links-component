<?php

namespace Drupal\usaid_link_sites\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class USAIDLinksHelpController to render the view of the page.
 */
class USAIDLinksHelpController extends ControllerBase {

  /**
   * Go to the help page.
   *
   * @inheritdoc
   */
  public function helpPage() {
    // Get the module path and set a variable.
    $module_handler = \Drupal::service('module_handler');
    $module_path = $module_handler->getModule('usaid_link_sites')->getPath();

    // Pull in the site config so we can get some global variables if needed.
    $site_config = \Drupal::service('config.factory')->getEditable('system.site');

    return [
      '#theme' => 'usaid_link_sites_help',
      '#config_name' => $site_config->getRawData()['name'],
      '#path' => $module_path,
    ];
  }

}
