<?php

namespace Drupal\usaid_link_sites\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure USAID Links settings for this site.
 *
 * @internal
 */
class USAIDLinksForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    // Instantiate the admin settings config.
    return [
      'usaid_link_sites.adminsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    // Set the form ID.
    return 'usaid_link_sites';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Build the form with the config settings.
    $config = $this->config('usaid_link_sites.adminsettings');

    // Select element that will determine the styling of the block.
    $form['usaid_links_style'] = [
      '#type' => 'select',
      '#options' => [
        'style_dark' => $this->t('Dark'),
        'style_light' => $this->t('Light'),
      ],
      '#prefix' => '<p>' . t('Use the settings below to choose a block style') . '</p>',
      '#required' => TRUE,
      '#title' => $this->t('Select a block style'),
      '#description' => $this->t('Choose a styling option from the list above.'),
      '#default_value' => $config->get('usaid_links_style'),
    ];

    // Width settings form item.
    $form['usaid_links_width'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Check this box to enable a full width region for this block. (Experimental)'),
      '#description' => $this->t('Note, use this setting with care. Ideally,
      your theme should have a full width region (not in a container) for this custom block to be placed.
      If for some reason you are not able to accomplish this, you may need to use this setting.
      See the help page for more information.'),
      '#return_value' => TRUE,
      '#default_value' => $config->get('usaid_links_width'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Submit handler for usaid_links_style.
    $this->config(
      'usaid_link_sites.adminsettings')->set('usaid_links_style',
      $form_state->getValue('usaid_links_style'))->save();

    // Submit handler for usaid_links_width.
    $this->config(
      'usaid_link_sites.adminsettings')->set('usaid_links_width',
      $form_state->getValue('usaid_links_width'))->save();
  }

}
