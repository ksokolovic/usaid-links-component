<?php

/**
 * @file
 * Functions to support the usaid_link_sites custom module.
 */

/**
 * Implements hook_theme().
 */
function usaid_link_sites_theme($existing, $type, $theme, $path): array {
  // Custom theme calls for templates within this module in /templates.
  return [
    'usaid_link_sites' => [
      'variables' => [
        'config_style' => '',
        'config_width' => '',
        'config_data' => '',
        'path' => '',
      ],
    ],
    'usaid_link_sites_help' => [
      'variables' => [
        'config_name' => '',
        'path' => '',
      ],
    ],
  ];
}

/**
 * Implements hook_preprocess_html().
 */
function usaid_link_sites_preprocess_html(&$vars) {
  // Add preconnect for google fonts.
  $head_links_google_api = [
    '#tag' => 'link',
    '#attributes' => [
      'rel' => 'preconnect',
      'href' => '//fonts.googleapis.com',
    ],
  ];

  $vars['page']['#attached']['html_head'][] = [
    $head_links_google_api,
    'head_links_google_api',
  ];

  // Add preconnect for google fonts.
  $head_links_google_gstatic = [
    '#tag' => 'link',
    '#attributes' => [
      'rel' => 'preconnect',
      'href' => '//fonts.gstatic.com',
      'crossorigin' => '',
    ],
  ];

  $vars['page']['#attached']['html_head'][] = [
    $head_links_google_gstatic,
    'head_links_google_gstatic',
  ];
}

/**
 * Implements hook_preprocess_block().
 */
function usaid_link_sites_preprocess_block(&$vars) {
  // Pull in the config from the module.
  $config = \Drupal::service('config.factory')->getEditable('usaid_link_sites.adminsettings');
  // Set a template variable.
  if (!empty($config->get("usaid_links_style"))) {
    $vars['has_links_style'] = TRUE;
  }
}
